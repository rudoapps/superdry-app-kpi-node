var express = require('express');
var session = require('cookie-session'); // Loads the piece of middleware for sessions
var bodyParser = require('body-parser'); // Loads the piece of middleware for managing the settings
var app = express();
var mysql = require('mysql');
var mssql = require('mssql');

// connection configurations
var mc = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'osaka2011',
    database: 'superdry'
});

var sqlConfig = {
  user: 'Fernando',
  password: 'S@l0m',
  server: 'superdry.bscorp.be',
  database: 'ES_Valencia',
  options: {
    encrypt: false
  }
}
 
// connect to database
mc.connect();

app.get('/tickets/today', function (req, res) {
  mssql.connect(sqlConfig, function() {
      var request = new mssql.Request();
      var today = new Date();
      var dd = today.getDate();
      var dy = today.getFullYear();
      var dm = today.getMonth() + 1;
      request.query('SELECT v.Factuurnummer AS ticket, v.Aantal AS quantity, v.Omschrijving AS description, v.Verkoopprijs AS price, v.origverkoopprijs AS original_price, v.TimeStamp AS date, v.Verkoper AS seller FROM VerkoopDetail AS v WHERE v.Factuurnummer IN(SELECT p.Factuurnummer FROM Verkoop AS p WHERE (DATEPART(yy, Datum) = ' + dy + ' AND DATEPART(mm, Datum) = ' + dm + ' AND DATEPART(dd, Datum) = ' + dd + '))',   function(error, result) {
        if (error) throw error;
        var arrayElements = [];
        
        recordset = result.recordset.map(each=>{
            var item = {}
            item["description"]     = each.description;
            item["quantity"]        = each.quantity;
            item["price"]           = each.price;
            item["original_price"]  = each.original_price;
            var found = false;
            for (var i = 0; i < arrayElements.length; i++){
                if (arrayElements[i].ticket == each.ticket){
                    arrayElements[i].total  = arrayElements[i].total + (item.price * item.quantity);
                    arrayElements[i].items.push(item);
                    found = true;
                }
            }
            if (!found) {
                var newElement = {};
                newElement["ticket"]    = each.ticket;
                newElement["date"]      = each.date;
                newElement["total"]     = item.price * item.quantity;
                newElement["seller"]    = each.seller;
                newElement["items"]     = [item];
                arrayElements.push(newElement);
            }
            return each;
        });
        mssql.close();
        return res.send({data: arrayElements});
        
        
      });
  });
  
})

app.get('/tickets', function (req, res) {
    mssql.connect(sqlConfig, function() {
        var request = new mssql.Request();
        var today = new Date();
        var dd = today.getDate()
        var dy = today.getFullYear();
        var dm = today.getMonth() + 1;
        request.query('SELECT Factuurnummer, Datum FROM Verkoop WHERE (DATEPART(yy, Datum) = ' + dy + ' AND DATEPART(mm, Datum) = ' + dm + ' AND DATEPART(dd, Datum) = ' + dd + ')',   function(error,result) {
          if (error) throw error;
          mssql.close();
          return res.send({data : result.recordset});
        });
    });
})

app.get('/tickets', function (req, res) {
    mssql.connect(sqlConfig, function() {
        var request = new mssql.Request();
        var today = new Date();
        var dd = today.getDate()
        var dy = today.getFullYear();
        var dm = today.getMonth() + 1;  
        request.query('SELECT Factuurnummer, Datum FROM Verkoop WHERE (DATEPART(yy, Datum) = ' + dy + ' AND DATEPART(mm, Datum) = ' + dm + ' AND DATEPART(dd, Datum) = ' + dd + ')',   function(error,result) {
          if (error) throw error;
          mssql.close();
          return res.send({data : result.recordset});
        });
    });
})

app.get('/payments', function (req, res) {
    mc.query('SELECT * FROM dash_payment', function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results});
    });
})

app.get('/payments/:id', function (req, res) {
    let payment_id = req.params.id;
    mc.query('SELECT * FROM dash_payment where id = ?', payment_id, function (error, results, fields) {
        if (error) throw error;
        return res.send({results});
    });
})

.listen(8080);